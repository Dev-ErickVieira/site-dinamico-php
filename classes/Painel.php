 <?php


class Painel
{

	public static function generateSlug($str){
		$str = mb_strtolower($str);
		$str = preg_replace('/(â|á|ã|à)/', 'a', $str);
		$str = preg_replace('/(ê|é)/', 'e', $str);
		$str = preg_replace('/(í|Í)/', 'i', $str);
		$str = preg_replace('/(ú)/', 'u', $str);
		$str = preg_replace('/(ó|ô|õ|Ô)/', 'o',$str);
		$str = preg_replace('/(_|\/|!|\?|#)/', '',$str);
		$str = preg_replace('/( )/', '-',$str);
		$str = preg_replace('/ç/','c',$str);
		$str = preg_replace('/(-[-]{1,})/','-',$str);
		$str = preg_replace('/(,)/','-',$str);
		$str=strtolower($str);
		return $str;
	}


	public static $cargos	= [
			'0' => 'Normal',
			'1' => 'Moderador',
			'2' => 'Administrador'
	];

	public static function logado(){
		return isset($_SESSION['login']) ? true : false;
	}

	public static function loggout(){
		setcookie('lembrar','true',time()-1,'/');
		session_destroy();
		header('Location: '.INCLUDE_PATH_PAINEL);
	}
	
	public static function carregarPagina(){
		if (isset($_GET['url'])) {
			$url = explode('/',$_GET['url']);
			if (file_exists('pages/'.$url[0].'.php')) {
				include('pages/'.$url[0].'.php');
			}else{
				header('Location: '.INCLUDE_PATH_PAINEL);
			}
		}else{
			include('pages/home.php');
		}
	}

	public static function listarUsuariosOnline(){
		self::limparUsuariosOnline();
		$sql = MySql::conectar()->prepare("SELECT * FROM `tb_admin_online`");
		$sql->execute();
		return $sql->fetchAll(PDO::FETCH_ASSOC);
	}

	public static function limparUsuariosOnline(){
		$date =  date("Y-m-d H:i:s");
		$sql = MySql::conectar()->exec("DELETE FROM `tb_admin_online` WHERE ultima_acao < '$date' - INTERVAL 1 MINUTE ");
	}

	public static function listarVisitas(){
		$sql = MySql::conectar()->prepare("SELECT * FROM `tb_admin_visitas`");
		$sql->execute();
		$results = $sql->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public static function listarVisitasHoje(){
		$sql = MySql::conectar()->prepare("SELECT * FROM `tb_admin_visitas` WHERE dia = ?");
		$sql->execute(array(date("Y-m-d")));
		$results = $sql->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public static function alertBox($tipo,$message){
		if ($tipo == 'sucesso') {
			echo "<div class=\"sucesso\"><i class=\"fas fa-check\"></i>$message</div>";
		}else if($tipo == 'erro'){
			echo "<div class=\"erro\"><i class=\"fas fa-times\"></i>$message</div>";
		}
	}

	public static function validarImagem($imagem){
		if ($imagem['type'] == 'image/jpeg' || 
			$imagem['type'] == 'image/jpg' ||
			$imagem['type'] == 'image/png'
	    ){	
	    	return true;
		}else{
			return false;
		}
	}

	public static function uploadFile($file){
		$infoArquivo = explode('.',$file['name']);
		$nomeImagem = uniqid().'-'.$infoArquivo[count($infoArquivo) - 2].'.'.$infoArquivo[count($infoArquivo) - 1];
		if(move_uploaded_file($file['tmp_name'],BASE_DIR_PAINEL.'/uploads/'.$nomeImagem)){
			return $nomeImagem;
		}else{
			return false;
		}
		
	}

	public static function deleteFile($file){
		@unlink(BASE_DIR_PAINEL.'uploads/'.$file);
	}

	public static function insert($array){
		
		// Para esse metodo funcionar corretamente ,
		//o formulario deve estar com a ordem de inputs igual as colunas
		//da tabela do banco de dados.

		$certo = true;
		$nome_tabela = $array['nome_tabela'];

		$query = "INSERT INTO `$nome_tabela` VALUES (NULL ";

		foreach ($array as $key => $value) {
			$nome = $key;
			$valor = $value;

			if ($nome == 'acao' || $nome == 'nome_tabela') {
				continue;
			}
			if ($value == '') {
				$certo = false;
				break;
			}
			$query.=",?";
			$parametros[] = $value;
		}

		$query.=")";
		
		if($certo == true){
			$sql = MySql::conectar()->prepare($query);
			$sql->execute($parametros);
			$order_id = MySql::conectar()->lastInsertId();
			$sql = MySql::conectar()->prepare("UPDATE `$nome_tabela` SET order_id = ? 
			WHERE id = $order_id");
			$sql->execute(array($order_id));
		}
		return $certo;
	}

	public static function update($array,$single = false){

		// Para esse metodo funcionar corretamente ,
		//o formulario deve estar com a ordem de inputs igual as colunas
		//da tabela do banco de dados.

		$certo = true;
		$first = false;
		
		$nome_tabela = $array['nome_tabela'];

		$query = "UPDATE `$nome_tabela` SET ";

		foreach ($array as $key => $value) {
			$nome = $key;
			$valor = $value;

			if ($nome == 'acao' || $nome == 'nome_tabela' || $nome == 'id' || $nome == 'foto_autor') {
				continue;
			}
			if ($value == '') {
				$certo = false;
				break;
			}
			if($first == false){
				$first = true;
				$query.="$nome=?";
			}else{
				$query.=",$nome=?";
			}

			$parametros[] = $valor;
		}
		
		if($certo == true){
			if ($single == false) {
				$parametros[] = $array['id'];
				$sql = MySql::conectar()->prepare($query.'WHERE id=?');
				$sql->execute($parametros);
			}else if($single == true){
				$sql = MySql::conectar()->prepare($query);
				$sql->execute($parametros);
			}
		}

		return $certo;
	}

	public static function selectAll($nome_tabela,$start = null,$end = null){
		if($start == null && $end == null){
			$depoimentos = MySql::conectar()->prepare("SELECT * FROM `$nome_tabela`
			ORDER BY order_id ASC");
			$depoimentos->execute();
		}
		else{
			$depoimentos = MySql::conectar()->prepare("SELECT * FROM `$nome_tabela` ORDER BY order_id ASC LIMIT $start,$end ");
			$depoimentos->execute();
		}
		return $results = $depoimentos->fetchAll(PDO::FETCH_ASSOC);
	}


	public static function deletar($tabela,$id = false){
		if ($id == false)
			$sql = MySql::conectar()->prepare("DELETE FROM `$tabela` ");
		else{
			$sql = MySql::conectar()->prepare("DELETE FROM `$tabela` WHERE id = $id ");
		}
		$sql->execute();
	}

	public static function updateDepoimento($table_name,$name,$data,$depoimento,$id){
		$sql = MySql::conectar()->prepare("UPDATE `$table_name` SET nome = ?,data = ?,depoimento = ? WHERE id = $id");
		if($sql->execute(array($name,$data,$depoimento)))
			return true;
		else
			return false;
	}

	public static function redirect($url){
		echo '<script>
			location.href="'.$url.'"
		</script>';
		die();
	}

	public static function select($table,$query,$arr){
		$sql = MySql::conectar()->prepare("SELECT * FROM `$table` WHERE $query ");
		$sql->execute($arr);
		return $sql->fetch(PDO::FETCH_ASSOC);
	}

	public static function orderItem($table,$orderType,$idItem){

		if($orderType == 'up'){

			$infoItemAtual = Painel::select($table,'id=?',array($idItem));

			$order_id = $infoItemAtual['order_id'];
			$itemBefore = MySql::conectar()->prepare("SELECT * FROM `$table` 
				WHERE order_id < $order_id ORDER BY order_id DESC LIMIT 1 ");

			$itemBefore->execute();
			$itemBefore = $itemBefore->fetch(PDO::FETCH_ASSOC);

			$sql = MySql::conectar()->prepare("UPDATE `$table` SET order_id = ?
			 WHERE id = ? ");

			$sql->execute(array($itemBefore['order_id'],$infoItemAtual['id']));

			$sql = MySql::conectar()->prepare("UPDATE `$table` SET order_id = ?
			 WHERE id = ? ");

			$sql->execute(array($infoItemAtual['order_id'],$itemBefore['id']));

		}else if($orderType == 'down'){

			$infoItemAtual = Painel::select($table,'id=?',array($idItem));

			$order_id = $infoItemAtual['order_id'];

			$itemAfter = MySql::conectar()->prepare("SELECT * FROM `$table` 
				WHERE order_id > $order_id ORDER BY order_id ASC LIMIT 1 ");

			$itemAfter->execute();
			$itemAfter = $itemAfter->fetch(PDO::FETCH_ASSOC);
			
			$sql = MySql::conectar()->prepare("UPDATE `$table` SET order_id = ?
			 WHERE id = ? ");

			$sql->execute(array($itemAfter['order_id'],$infoItemAtual['id']));

			$sql = MySql::conectar()->prepare("UPDATE `$table` SET order_id = ?
			 WHERE id = ? ");

			$sql->execute(array($infoItemAtual['order_id'],$itemAfter['id']));
			
		}
	}	



}
?>
