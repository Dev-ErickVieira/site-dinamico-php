<?php

require_once('vendor/autoload.php');

use PHPMailer\PHPMailer\PHPMailer;

class Email
{	
	private $mail;

	//host = 'smtp.gmail.com';
	//username = "erickjoaoteste@gmail.com";
	//senha = "<?senha>";

	public function __construct($host,$username,$password,$name)
	{

		$this->mail = new PHPMailer;

		$this->mail->isSMTP();
		$this->mail->SMTPDebug = 0;
		$this->mail->Host = $host;
		$this->mail->Port = 587;
		$this->mail->SMTPSecure = 'tls';
		$this->mail->SMTPAuth = true;
		$this->mail->Username = $username;
		$this->mail->Password = $password;
		$this->mail->setFrom($username,$name);
		$this->mail->CharSet = 'UTF-8';
	}

	public function addAddress($email,$name){
		$this->mail->addAddress($email,$name);
	}

	public function formatarEmail($info){
		$this->mail->Subject = $info['assunto'];

		$this->mail->msgHTML($info['corpo']);

		$this->mail->AltBody = strip_tags($info['corpo']);
	}

	public function enviarEmail(){
		if ($this->mail->send()) 
		  	return true;
		else
			echo "Mailer Error: " . $this->mail->ErrorInfo;
	}

}

?>

