<?php
	$url = isset($_GET['url']) ? $_GET['url'] : '';
	if ($url == 'loggout') {
		Painel::loggout();
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
	<link href="https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/style.css">
	<title>Painel De Controle</title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0">
	<meta name="X-UA-Compatible" content="IE=Edge">
</head>
<body>

	<aside>
		<div class="box-usuario">
			<?php
				if ($_SESSION['img'] == '') {
			?>
			<div class="avatar-usuario">
				<i class="fas fa-user"></i>
			</div><!--avatar-usuario-->
			<?php }else{
				?>
			<div class="imagem-usuario">
				<img src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $_SESSION['img']?>">
			</div><!--avatar-usuario-->
			<?php } ?>
			<div class="nome-usuario">
				<p><?php echo $_SESSION['nome']; ?></p>
				<p><?php echo pegaCargo($_SESSION['cargo']); ?></p>
			</div><!--nome-usuario-->
		</div><!--box-usuario-->
		<div class="itens-menu">
			<h2>Cadastro</h2>
			<a <?php selecionadoMenu('cadastrar-depoimento'); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>cadastrar-depoimento"><i class="fas fa-angle-double-right"></i>Cadastrar Depoimento</a>
			<a <?php selecionadoMenu('cadastrar-servico'); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>cadastrar-servico"><i class="fas fa-angle-double-right"></i>Cadastrar Serviço</a>
			<a <?php selecionadoMenu('cadastrar-slides'); ?>  href="<?php echo INCLUDE_PATH_PAINEL ?>cadastrar-slides"><i class="fas fa-angle-double-right"></i>Cadastrar Slides</a>
			<h2>Gestão</h2>
			<a <?php selecionadoMenu('listar-depoimentos'); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>listar-depoimentos"><i class="fas fa-angle-double-right"></i>Listar Depoimentos</a>
			<a <?php selecionadoMenu('listar-servicos'); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>listar-servicos"><i class="fas fa-angle-double-right"></i>Listar Serviços</a>
			<a <?php selecionadoMenu('listar-slides'); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>listar-slides"><i class="fas fa-angle-double-right"></i>Listar Slides</a>
			<h2>Administração</h2>
			<a <?php selecionadoMenu('editar-usuario'); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>editar-usuario"><i class="fas fa-angle-double-right"></i>Editar Usuário</a>
			<a <?php selecionadoMenu('adicionar-usuario'); ?> <?php verificaPermissaoMenu(2); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>adicionar-usuario"><i class="fas fa-angle-double-right"></i>Adicionar Usuários</a>
			<h2>Configuração Geral</h2>
			<a <?php selecionadoMenu('editar-site'); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>editar-site"><i class="fas fa-angle-double-right"></i>Editar Site</a>
			<h2>Gestão de Notícias</h2>	
			<a <?php selecionadoMenu('cadastrar-categoria'); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>cadastrar-categoria"><i class="fas fa-angle-double-right"></i>Cadastrar Categoria</a>
			<a <?php selecionadoMenu('gerenciar-categorias'); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>gerenciar-categorias"><i class="fas fa-angle-double-right"></i>Gerenciar Categoria</a>
			<a <?php selecionadoMenu('cadastrar-noticia'); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>cadastrar-noticia"><i class="fas fa-angle-double-right"></i>Cadastrar noticias</a>
			<a <?php selecionadoMenu('gerenciar-noticias'); ?> href="<?php echo INCLUDE_PATH_PAINEL ?>gerenciar-noticias"><i class="fas fa-angle-double-right"></i>Gerenciar Noticias</a>
		</div><!--itens-menu-->
	</aside>
	<header>
		<div class="container">
			<div class="menu-btn">
				<i class="fa fa-bars"></i>
			</div><!--menu-btn-->
			<div class="loggout">
				<a href="<?php echo INCLUDE_PATH_PAINEL ?>loggout"><i class="fas fa-window-close"></i></i><span>Sair </span> </a>
			</div><!--loggout-->
			<div class="menu-home">
				<i class="fas fa-home"></i>
				<a href="<?php echo INCLUDE_PATH_PAINEL ?>">Pagina inicial</a>
			</div>
			<div class="clear"></div><!--cler-->
		</div><!--container-->
</header>
<div class="content">

	<?php
		Painel::carregarPagina();
	?>

	
</div><!--content-->

<div class="clear"></div><!--clear-->

<script src="<?php ECHO INCLUDE_PATH_PAINEL ?>js/jquery.js"></script>
<script src="<?php echo INCLUDE_PATH ?>js/config.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL ?>js/main.js"></script>
 <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
  <script>
  tinymce.init({
    selector: '.tinymce',
    plugins: "image",
    height:300
  });
  </script>
</body>
</html>