<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Editar Notícia</h2>
		<div class="form-editar-usuario">

			<?php

				$id = isset($_GET['id']) ? $_GET['id'] : false;

				if ($id == false) {
					Painel::alertBox('erro','Vc precisa passar o parametro ID');
					die();
				}

				$sql = MySql::conectar()->prepare("SELECT * FROM `tb_site_noticias` WHERE id = $id ");
				$sql->execute();
				$result = $sql->fetch(PDO::FETCH_ASSOC);

				if (isset($_POST['acao'])) {
					$categoria_id = $_POST['categoria_id'];
					$titulo = $_POST['titulo'];
					$conteudo = $_POST['conteudo'];
					$capa = $_FILES['capa'];
					$capa_atual = $result['capa'];

					$verifica = MySql::conectar()->prepare("SELECT `id` FROM `tb_site_noticias` WHERE titulo = ? AND categoria_id = ? AND id != ? ");
					$verifica->execute(array($titulo,$categoria_id,$id));

					if($verifica->rowCount() == 0){
						if ($capa['name'] != '') {
							
							if (Painel::validarImagem($capa)){

								Painel::deleteFile($capa_atual);
								$capa = Painel::uploadFile($capa);
								$slug = Painel::generateSlug($titulo);
								$arr = [
								'categoria_id'=>$categoria_id,
								'titulo'=>$titulo,
								'conteudo'=>$conteudo,
								'capa'=>$capa,
								'slug'=>$slug,
								'nome_tabela'=>'tb_site_noticias',
								'id'=>$id
								];

								if(Painel::update($arr) == true)
									Painel::alertBox('sucesso','Notícia cadastrada com sucesso');
								else
									Painel::alertBox('erro','não foi possivel fazer a alteração');
							}else{

								Painel::alertBox('erro','formato do arquivo errado');
							}
						}else{
							$slug = Painel::generateSlug($titulo);
							$arr = [
								'categoria_id'=>$categoria_id,
								'titulo'=>$titulo,
								'conteudo'=>$conteudo,
								'slug'=>$slug,
								'nome_tabela'=>'tb_site_noticias',
								'id'=>$id
								];
							if(Painel::update($arr) == true)
								Painel::alertBox('sucesso','Notícia cadastrada com sucesso');
							else
								Painel::alertBox('erro','não foi possivel fazer a alteração');
						}
					}else{
						Painel::alertBox('erro','Este título já está em uso.');
					}

					$sql = MySql::conectar()->prepare("SELECT * FROM `tb_site_noticias` WHERE id = $id ");
					$sql->execute();
					$result = $sql->fetch(PDO::FETCH_ASSOC);
				}
			?>

			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Categoria</label>
					<select name="categoria_id">
						<?php
							$categoria = Painel::selectAll('tb_site_categorias');
							foreach ($categoria as $key => $value) {
						?>
						<option <?php if($value['id'] == $result['categoria_id']) echo 'selected'; ?> value="<?php echo $value['id'] ?>"><?php echo $value['nome']; ?></option>
						<?php } ?>
					</select>
				</div><!--form-group-->
				<div class="form-group">
					<label>Título: </label>
					<input type="text" name="titulo" value="<?php echo $result['titulo']; ?>" required>
				</div><!--form-group-->
				<div class="form-group">
					<label>Capa: </label>
					<input type="file" name="capa">
				</div><!--form-group-->	
				<div class="form-group">
					<label>Conteúdo: </label>
					<textarea name="conteudo"><?php echo $result['conteudo']; ?></textarea>
				</div><!--form-group-->	
				<div class="form-group">
					<input type="submit" name="acao" value="Atualizar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->