<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Cadastrar Notícia</h2>
		<div class="form-editar-usuario">

			<?php

				function recoverPost($var){
					if (isset($_POST[$var])) {
						echo $_POST[$var];
					}else{
						echo '';
					}
				}

				if (isset($_POST['acao'])) {
					//enviei o formulario
					$categoria_id = $_POST['categoria_id'];
					$titulo = $_POST['titulo'];
					$conteudo = $_POST['conteudo'];
					$capa = $_FILES['capa'];
					$order_id = $_POST['order_id'];
					$verifica = MySql::conectar()->prepare("SELECT `id` FROM `tb_site_noticias` WHERE titulo = ? AND categoria_id = ? ");
					$verifica->execute(array($titulo,$categoria_id));
					$date = date("Y-m-d");
					if ($verifica->rowCount() == 0) {
						if($capa['name'] != ''){
							//existe o upload de imagem.
							if(Painel::validarImagem($capa)){
								include('../classes/lib/WideImage.php');
									$slug = Painel::generateSlug($titulo);
									$capa = Painel::uploadFile($capa);
									WideImage::load('uploads/'.$capa)->resize(720)->saveToFile('uploads/'.$capa);
									$sql = MySql::conectar()->prepare("INSERT INTO `tb_site_noticias` VALUES (NULL,?,?,?,?,?,?,?) ");
									$sql->execute(array($categoria_id,$date,$titulo,$conteudo,$capa,$slug,$order_id));
									$order_id = MySql::conectar()->lastInsertId();
									$sql = MySql::conectar()->prepare("UPDATE `tb_site_noticias` SET order_id = ? WHERE id = $order_id ");
									$sql->execute(array($order_id));
									Painel::redirect(INCLUDE_PATH_PAINEL.'cadastrar-noticia?sucesso=true');
							}else{
								Painel::alertBox("erro","Imagem invalida | Verifique o tamanho da imagem ou o formato do arquivo");
							}
						}else{
							Painel::alertBox("erro","Imagem não encontrada");
						}
					}else{
						Painel::alertBox('erro','Este título já está em uso nessa categoria.');
					}
				}
					
				if (isset($_GET['sucesso']) && !isset($_POST['acao'])) {
					Painel::alertBox('sucesso','Notícia cadastrada com sucesso');
				}
			?>

			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Categoria</label>
					<select name="categoria_id">
						<?php
							$categoria = Painel::selectAll('tb_site_categorias');
							foreach ($categoria as $key => $value) {
						?>
						<option <?php if($value['id'] == @$_POST['categoria_id']) echo 'selected'; ?> value="<?php echo $value['id'] ?>"><?php echo $value['nome']; ?></option>
						<?php } ?>
					</select>
				</div><!--form-group-->
				<div class="form-group">
					<label>Título</label>
					<input type="text" name="titulo" required value="<?php recoverPost('titulo') ?>">
				</div><!--form-group-->
				<div class="form-group">
					<label>Conteúdo</label>
					<textarea class="tinymce" name="conteudo"><?php recoverPost('conteudo'); ?></textarea>
				</div><!--form-group-->
				<div class="form-group">
					<label>Capa</label>
					<input type="file" name="capa">
				</div><!--form-group-->
				<div class="form-group">
					<input type="hidden" name="order_id" value="0">
					<input type="hidden" name="nome_tabela" value="tb_site_noticias">
					<input type="submit" name="acao" value="Cadastrar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->