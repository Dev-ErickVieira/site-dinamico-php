<?php
	verificaPermissaoPagina(2);
?>
<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Adicionar Usuário</h2>
		<div class="form-editar-usuario">

			<?php	
				if (isset($_POST['acao'])) {

					$login = $_POST['login'];
					$nome = $_POST['nome'];
					$senha = $_POST['password'];
					$cargo = $_POST['cargo'];
					$imagem = $_FILES['imagem'];

					if ($login == '') {
						Painel::alertBox('erro','O login está vazio !');
					}else if($nome == ''){
						Painel::alertBox('erro','O nome está vazio !');
					}else if($senha == ''){
						Painel::alertBox('erro','A senha está vazia !');
					}else if($imagem == ''){
						Painel::alertBox('erro','A imagem precisa estar selecionada !');
					}else if($cargo == ''){
						Painel::alertBox('erro','O cargo precisa estar selecionado !');
					}else{
						//Podemos cadastrar!
						if ($cargo >= $_SESSION['cargo']) {
							Painel::alertBox('erro','Você precisa selecionar um cargo menor que o seu !');
						}else if(Painel::validarImagem($imagem) == false){
							Painel::alertBox('erro','O formato especificado não esta correto');
						}else if(Usuario::userExists($login)){
							Painel::alertBox('erro','O Login ja existe, escolha outro login');
						}else{
							//Inserir no banco de dados e concluir o cadastro!
							$usuario = new Usuario();
							$imagem = Painel::uploadFile($imagem);
							$usuario->cadastrarUsuario($login,$senha,$imagem,$nome,$cargo);
							Painel::alertBox('sucesso','O cadastro do usuario '.$login.' foi feito com sucesso! ');
						}
					}

				}
			?>

			<form method="post" enctype="multipart/form-data" action="">
				<div class="form-group">
					<label>Login</label>
					<input type="text" name="login"
				</div><!--form-group-->
				<div class="form-group">
					<label>Nome</label>
					<input type="text" name="nome">
				</div><!--form-group-->
				<div class="form-group">
					<label>Senha</label>
					<input type="password" name="password">
				</div><!--form-group-->
				<div class="form-group">
					<label>Cargo:</label>
					<select name="cargo">
						<?php
						foreach (Painel::$cargos as $key => $value) {
							if ($key < $_SESSION['cargo']) 
								echo '<option value="'.$key.'">'.$value.'</option>';
						}
						?>
					</select>
				</div><!--form-group-->
				<div class="form-group">
					<label>Imagem</label>
					<input type="file" name="imagem">
				</div><!--form-group-->
				<div class="form-group">
					<input type="submit" name="acao" value="Atualizar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->