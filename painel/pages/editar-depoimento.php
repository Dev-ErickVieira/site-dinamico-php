<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Editar Depoimento</h2>
		<div class="form-editar-usuario">

			<?php

				$id = isset($_GET['id']) ? $_GET['id'] : false;

				if ($id == false) {
					Painel::alertBox('erro','Um erro foi encontrado');
					die();
				}

				$sql = MySql::conectar()->prepare("SELECT * FROM `tb_site_depoimentos` WHERE id = $id ");
				$sql->execute();
				$result = $sql->fetch(PDO::FETCH_ASSOC);

				if (isset($_POST['acao'])) {
					$newNome = $_POST['nome'];
					$newDepoimento = $_POST['depoimento'];
					$date = $_POST['data'];
					if (Painel::updateDepoimento('tb_site_depoimentos',$newNome,$date,$newDepoimento,$id)) {
						Painel::alertBox('sucesso','O cadastro do depoimento foi realizado com sucesso');
					}else{
						Painel::alertBox('erro','Campos vazios não são permitidos.');
					}
				}
			?>

			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Nome : </label>
					<input type="text" name="nome" value="<?php echo $result['nome']; ?>" required>
				</div><!--form-group-->
				<div class="form-group">
					<label>Data</label>
					<input type="text" name="data" value="<?php echo date("d/m/Y"); ?>" required>
				</div><!--form-group-->
				<div class="form-group">
					<label>Depoimento</label>
					<textarea name="depoimento""><?php echo $result['depoimento'] ?></textarea>
				</div><!--form-group-->
				<div class="form-group">
					<input type="hidden" name="nome_tabela" value="tb_site_depoimentos">
					<input type="submit" name="acao" value="Atualizar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->