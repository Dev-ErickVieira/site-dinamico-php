<?php
	$usuariosOnline = Painel::listarUsuariosOnline();
	$visitas = Painel::listarVisitas();
	$visitasHoje = Painel::ListarVisitasHoje();
?>
<div class="box-content w100">
	<div class="box-metricas">
		<h2><i class="fas fa-home"></i>Painel De Controle</h2>
		<div class="box-metricas-single">
			<div class="box-metricas-single-wraper">
				<h2>Usuários Online</h2>
				<p><?php echo count($usuariosOnline); ?></p>
			</div><!--box-metricas-single-wrapper-->
		</div><!--metricas-single-->
		<div class="box-metricas-single">
			<div class="box-metricas-single-wraper">
				<h2>Total de Visitas</h2>
				<p><?php echo count($visitas) ?></p>
			</div><!--box-metricas-single-wrapper-->
		</div><!--metricas-single-->
		<div class="box-metricas-single">
			<div class="box-metricas-single-wraper">
				<h2>Visitas Hoje</h2>
				<p><?php echo count($visitasHoje) ?></p>
			</div><!--box-metricas-single-wrapper-->
		</div><!--metricas-single-->
	</div><!--box-metricas-->
</div> <!--box-content-->

<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-users"></i>Úsuarios Online</h2>
		<div class="table-responsive">
			<div class="row">
				<div class="col col-2">
					<span>IP</span>
				</div><!--col-->
				<div class="col col-2">
					<span>Última ação</span>
				</div><!--col-->
			</div><!--row-->
			<?php
				foreach ($usuariosOnline as $key => $value) {
			?>
			<div class="row">
				<div class="col col-2">
					<span><?php echo $value['ip']; ?></span>
				</div><!--col-->
				<div class="col col-2">
					<span><?php echo date("d/m/Y H:i:s",strtotime($value['ultima_acao'])); ?></span>
				</div><!--col-->
			</div><!--row-->
			<?php } ?>
		</div><!--table-responsive-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->

<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fab fa-teamspeak"></i>Usuários do Painel</h2>
		<div class="table-responsive">
			<div class="row">
				<div class="col col-2">
					<span>Nome</span>
				</div><!--col-->
				<div class="col col-2">
					<span>Cargo</span>
				</div><!--col-->
			</div><!--row-->
			<?php
				$usuariosPainel = MySql::conectar()->prepare("SELECT * FROM `tb_admin`");
				$usuariosPainel->execute();
				$usuariosPainel = $usuariosPainel->fetchAll(PDO::FETCH_ASSOC);
				foreach ($usuariosPainel as $key => $value) {
			?>
			<div class="row">
				<div class="col col-2">
					<span><?php echo $value['nome']; ?></span>
				</div><!--col-->
				<div class="col col-2">
					<span><?php echo pegaCargo($value['cargo']); ?></span>
				</div><!--col-->
			</div><!--row-->
			<?php } ?>
		</div><!--table-responsive-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->
