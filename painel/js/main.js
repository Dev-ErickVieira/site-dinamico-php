$(function(){

	var open = true;
	var windowSize = $(window)[0].innerWidth; 

	var targetSizeMenu = (windowSize <= 600) ? 150 : 300;

	if (windowSize <= 768) {
		open = false;
	}else{
		open = true;
	}

	$('.menu-btn').click(()=>{
		if (open) {
			// o menu esta aberto.
			$('aside').animate({'width':0,'padding':0},()=>{
				open = false;
			});
			$('.content,header').css('width','100%');
			$('.content,header').animate({'left':0},()=>{
				open = false;
			});
		}else{
			//o menu esta fechado.
			$('aside').css('display','block');
			$('aside').animate({'width':targetSizeMenu+'px','padding':'20px 0'},()=>{
				open = true; 
			});

			if (windowSize <= 600) {
				$('.content,header').css('width','calc(100% - 150px)');
				$('.content').css('padding','0');
			}else{
				$('.content,header').css('width','calc(100% - 300px)');
			}

			$('.content,header').animate({'left':targetSizeMenu+'px'},()=>{
				open = true;
			});
		}
	});

	$("[actionExcluir=delete]").click(function(){
		var r = confirm("Deseja excluir ?");
		if (r == true) {
			return true;
		}else{	
			return false;
		}
	});
	
})