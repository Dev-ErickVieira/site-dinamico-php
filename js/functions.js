$(function(){

	$('nav.mobile i').click(function(){

		var listaMenu = $('nav.mobile ul');
		var button = $('#botao');

		if (listaMenu.is(':hidden') == true) {
			button.removeClass("fa-bars");
			button.addClass("fa-times");
			listaMenu.slideToggle();

		} else{
			button.removeClass("fa-times");
			button.addClass("fa-bars");
			listaMenu.slideToggle();
		}

	});

	if ($('target').length > 0) {

		//o elemento existe portanto precisamos dar o scroll em algum elemento.
		var elemento = '#'+$('target').attr('target');

		var divScroll = $(elemento).offset().top;

		$('html,body').animate({'scrollTop':divScroll},1000);
	}

	/*
	carregarDinamico();

	function carregarDinamico(){

		$('[realtime]').click(function(){
			var pagina = $(this).attr('realtime');
			$('.container-principal').html(' ');
			$('.container-principal').load('http://localhost/Projetos%20PHP/projeto_01/pages/'+pagina+'.php');
			
			return false;
		});
	}
	*/

});