$(function(){


	var curSlide = 0;

	var maxSlide = ( $('.single-banner').length - 1);

	var timer;

	var delay = 5;

	initSlider();
	changeSlide(true);

	function initSlider(){

		$('.single-banner').hide();
		$('.single-banner').eq(0).show();
		var content = $('.bullets');
		var span = '<span></span>';
		for(var i = 0; i <= maxSlide; i++){
			if (i == 0) 
				$('.bullets').append('<span class="active-slider"></span>');
			 else
				$('.bullets').append('<span></span>');
		}
	}

	function changeSlide(executa){
			
		if (executa == true) {

				timer = setInterval(function(){

				$('.single-banner').eq(curSlide).stop().fadeOut(2000);
				$('.bullets > span').eq(curSlide).css('background-color','#444');

				curSlide++;
				if (curSlide > maxSlide) {
					curSlide = 0;
				}
				$('.single-banner').eq(curSlide).stop().fadeIn(2000);

				$('.bullets > span').eq(curSlide).css('background-color','white');
			},delay * 1000);
		} else{
			clearInterval(timer);
		}
		
	}

	$('body').on('click','.bullets span',function(){
		changeSlide(false);
		var curBullet = $(this);
		$('.single-banner').eq(curSlide).stop().fadeOut(2000);
		curSlide = curBullet.index();
		$('.single-banner').eq(curSlide).stop().fadeIn(2000);
		changeSlide(true);
		$('.bullets span').css('background-color','#444');
		curBullet.css('background-color','white');
	});

})