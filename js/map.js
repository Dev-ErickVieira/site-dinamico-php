$(function(){
	var map;

	//inicializa o Mapa

	function initialize(){

		var mapProp = {

			center:new google.maps.LatLng(-22.933216,-43.238145),
			scrollWheel:false,
			zoom:12,
			styles:[{
				stylers:[{
					saturation:-50
				}]
			}],
			mapTypeId:google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById("map"),mapProp);
	}

	//Adiciona uma Marca no Mapa
	
	function addMarker(lat,long,icon,content,ShowInfoWindow,openInfoWindow){
		var latLng = {
			lat:lat,
			lng:long
		};

		var marker = new google.maps.Marker({
			position:latLng,
			map:map,
			icon:icon
		});

		var infoWindow = new google.maps.InfoWindow({
			content:content,
			maxWidth:600,
			pixelOffset:new google.maps.Size(0,10)
		});


		if (ShowInfoWindow == undefined) {
			infoWindow.open(map,marker);
			google.maps.event.addListener(marker,'click',function(){
				infoWindow.open(map,marker);
			});
		} else if(openInfoWindow == true){
			infoWindow.open(map,marker);
		}

	}


	
	var conteudo = '<p style="color:black;font-size:16px;border-bottom:1px solid orange;">Empresa</p>';
	initialize();
	addMarker(-22.918643,-43.236554,'',conteudo,undefined,true);
	
})