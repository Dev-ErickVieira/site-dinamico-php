 <section class="contato" style="position: relative;">
	<div class="container">
		<div class="alert alert-success" pag="contato" role="alert">
  			<strong>Email enviado com sucesso</strong> 
		</div>
		<div class="contato-title">
			<h2>Entre em Contato</h2>
			<div class="contato-line"></div><!--contato-line-->
		</div><!--contato-title-->
		
		<form method="POST" action="" class="ajax-form">
			<input type="text" name="nome" placeholder="Nome.." required="">
			<input type="text" name="email" placeholder="E-Mail.." required="">
			<input type="text" name="telefone" placeholder="telefone.." required="">
			<textarea placeholder="Mensagem..." name="mensagem"></textarea>
			<input type="submit" name="acao" value="Enviar">
		</form>
		
	</div><!--container-->
</section><!--contato-->
<section class="mapa">
	<div id="map">
		
	</div>
</section><!--mapa-->