
<section class="banner-container">
	<?php
		$sql = MySql::conectar()->prepare('SELECT * FROM `tb_site_slides`');
		$sql->execute();
		$slides = $sql->fetchAll(PDO::FETCH_ASSOC);

		foreach ($slides as $key => $value) {
	 ?>
		<div style="background-image: url('<?php echo INCLUDE_PATH_PAINEL?>uploads/<?php echo $value['slide'] ?>');" class="single-banner"></div>

	<?php } ?>
	<div class="container">
		<div class="banner-email">
			
			
			<form method="POST" action="" class="ajax-form">
				<h2>Qual o seu melhor e-mail?</h2>
				<input type="email" placeholder="Email.." name="email" required="">
				<input type="submit" name="acao" value="Cadastrar">
			</form>
		</div><!--banner-email-->
		<div class="alert alert-success" role="alert">
  			<strong>Email enviado com sucesso</strong> 
		</div>
	</div><!--container-->
	<div class="bullets">
		
	</div><!--bullets-->
</section><!--banner-container-->

	<section class="descricao-autor">
		<div class="container">
			<div class="descricao-texto">
				<h2><?php echo $infoSite['nome_autor']; ?></h2>
				<p><?php echo $infoSite['descricao']; ?></p>
			</div><!--descricao-texto-->
			<div class="descricao-imagem">
				<img  src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $infoSite['foto_autor'] ?>">
			</div><!--descricao-imagem-->
			<div class="clear"></div>
		</div><!--container-->
	</section><!--descricao-autor-->

	<section class="especialidades">
		<div class="container">
			<h2 class="title">Especialidades</h2>
			<div class="box-especialidades">
				<h2><i class="<?php echo $infoSite['icone_1']; ?>"></i></h2>
				<h3><?php echo $infoSite['titulo_1']; ?></h3>
				<p><?php echo $infoSite['descricao_1']; ?></p>
			</div><!--box-especialidades-->
			<div class="box-especialidades">
				<h2><i class="<?php echo $infoSite['icone_2']; ?>"></i></h2>
				<h3><?php echo $infoSite['titulo_2']; ?></h3>
				<p><?php echo $infoSite['descricao_2']; ?></p>
			</div><!--box-especialidades-->
			<div class="box-especialidades">
				<h2><i class="<?php echo $infoSite['icone_3']; ?>"></i></h2>
				<h3><?php echo $infoSite['titulo_3']; ?></h3>
				<p><?php echo $infoSite['descricao_3']; ?></p>
			</div><!--box-especialidades-->
			<div class="clear"></div>
		</div><!--container-->
	</section><!--especialidades-->

	<section class="extras">
		<div id="Depoimentos" class="container">
			<div class="depoimentos">
				<h2  class="title">depoimentos</h2>
				<?php 
						$sql = MySql::conectar()->prepare("SELECT * FROM `tb_site_depoimentos` ORDER BY order_id ASC LIMIT 3");
						$sql->execute();
						$results = $sql->fetchAll(PDO::FETCH_ASSOC);
						foreach ($results as $key => $value) {
					?>
						<div class="depoimentos-single">
							<p class="depoimento-descricao"><?php echo $value['depoimento'] ?></p>
							<p class="nome-autor"><?php echo $value['nome'] ?> - <?php echo $value['data'] ?></p>
						</div><!--depoimentos--single-->
					<?php } ?>
			</div><!--depoimentos-->
			<div id="Servicos" class="servicos">
				<h2 class="title">Serviços</h2>
				<ul>
					<?php
					
						$sql = MySql::conectar()->prepare("SELECT * FROM `tb_admin_servicos` ORDER BY order_id ASC LIMIT 3 ");
						$sql->execute();
						$results = $sql->fetchAll(PDO::FETCH_ASSOC);
						foreach ($results as $key => $value) {
							echo '<li> '.$value['servico'].' </li>';
						}
					 ?>
				</ul>
			</div><!--servicos-->
			<div class="clear"></div>
		</div><!--container-->
	</section><!--extras-->