<?php 
	$slug_categoria = explode('/',$_GET['url'])[1];
	$verifica_categoria = MySql::conectar()->prepare("SELECT * FROM  `tb_site_categorias` WHERE slug = ?");
	$verifica_categoria->execute(array($slug_categoria));
	if ($verifica_categoria->rowCount() == 0) {
		Painel::redirect(INCLUDE_PATH.'/noticias');
	}else{
		$categoria_info = $verifica_categoria->fetch();
		$slug_noticia = explode('/',$_GET['url'])[2];
		$verifica_noticia = MySql::conectar()->prepare("SELECT `slug` FROM `tb_site_noticias` WHERE slug = ? ");
		$verifica_noticia->execute(array($slug_noticia));
		if ($verifica_noticia->rowCount() == 0) {
			Painel::redirect(INCLUDE_PATH.'/noticias');
		}else{
			$post = MySql::conectar()->prepare("SELECT * FROM `tb_site_noticias` WHERE slug = ? AND categoria_id = ?");
			$post->execute(array($slug_noticia,$categoria_info['id']));
			$post = $post->fetch();
?>
<section class="noticia-single">
	<div class="container">
		<div class="header-noticia">
			<h1><?php echo $post['titulo']; ?></h1>
		</div>
		<div class="noticia-single-img">
			<img src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $post['capa']; ?>">
		</div><!--noticia-single-img-->
		<article>
			<div class="container">
				<p><?php echo $post['conteudo']; ?></p>	
			</div>
		</article>
	</div><!--container-->
</section><!--noticia-single-->

<?php
	}
}
?>
