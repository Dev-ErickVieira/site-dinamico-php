 <?php include('config.php'); ?>
<?php Site::updateUsuarioOnline(); ?>
<?php Site::contador(); ?>
<?php
	$infoSite = MySql::conectar()->prepare("SELECT * FROM `tb_site_config`");
	$infoSite->execute();
	$infoSite = $infoSite->fetch();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="icon" type="image/x-icon" href="<?php echo INCLUDE_PATH ?>images/favicon.ico">
	<link rel="stylesheet" type="text/css" href="<?php echo INCLUDE_PATH ?>css/style.css">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=EDGE">
	<meta name="keywords" content="palavras-chave,do,meu,site">
	<meta name="description" content="meu primeiro site dinamico">
	<meta name="author" content="Erick Vieira Serra">	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<title><?php echo $infoSite['titulo']; ?></title>
</head>
<body>
	<base base="<?php echo INCLUDE_PATH; ?>" />

	<?php
		$url = isset($_GET['url']) ? $_GET['url'] : 'home';
		
		switch ($url) {
			case 'Depoimentos':
				echo '<target target="Depoimentos" />';
				break;

			case 'Servicos':
				echo '<target target="Servicos" />';
				break;
		}
	?>

	<div class="overlay-loading">
		<img src="<?php echo INCLUDE_PATH ?>images/ajax-loader.gif">
	</div><!--overlay-loading-->
	
	<header>
		<div class="container">
			<div class="logo"></div><!--logo-->
			<nav class="desktop">
				<ul>
					<li><a href="<?php echo INCLUDE_PATH; ?>">Home</a></li>
					<li><a href="<?php echo INCLUDE_PATH; ?>Depoimentos">Depoimentos</a></li>
					<li><a href="<?php echo INCLUDE_PATH; ?>noticias">Notícias</a></li>
					<li><a href="<?php echo INCLUDE_PATH ?>Servicos">Serviços</a></li>
					<li><a href="<?php echo INCLUDE_PATH ?>Contato">Contato</a></li>
				</ul>
			</nav><!--desktop-->
			<div class="clear"></div>
			<nav class="mobile">
				<i id="botao" class="fas fa-bars" aria-hidden="true"></i>
				<div class="clear"></div>
				<ul>
					<li><a href="<?php echo INCLUDE_PATH ?>">Home</a></li>
					<li><a href="<?php echo INCLUDE_PATH ?>Depoimentos">Depoimentos</a></li>
					<li><a href="<?php echo INCLUDE_PATH; ?>noticias">Notícias</a></li>
					<li><a href="<?php echo INCLUDE_PATH ?>Servicos">Serviços</a></li>
					<li><a href="<?php echo INCLUDE_PATH ?>Contato">Contato</a></li>
				</ul>
			</nav><!--desktop-->
		</div><!--container-->
	</header>
	<div class="container-principal">
	<?php

		if (file_exists('pages/'.$url.'.php')) {
			include('pages/'.$url.'.php');
		}else{
			if ($url != 'Depoimentos' && $url != 'Servicos') {
				$urlPar = explode('/',$url);
				if ($urlPar[0] == 'noticias') {
					include('pages/noticias.php');
				}else{
					include('pages/erro404.php');
				}	
			} else{
				include('pages/home.php');
			}
		}
	?>
	</div><!--conteudo-principal-->

	<footer>
		<div class="container">
			<p>Todos os direitos reservados</p>
		</div><!--container-->
	</footer>

<!--google maps API-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbDSK5mZ1wG93JZ7WVnBh-5Mla_j7kqGg&callback=initMap"></script>



<script src="<?php echo INCLUDE_PATH ?>js/jquery.js"></script>
<script src="<?php echo INCLUDE_PATH ?>js/config.js"></script>

<script src="<?php echo INCLUDE_PATH ?>js/map.js"></script>
<script src="<?php echo INCLUDE_PATH ?>js/functions.js"></script>


<?php
	if ($url == 'home' || $url == '') {
?>
<script src="<?php echo INCLUDE_PATH ?>js/slides.js"></script>
<script src="<?php echo INCLUDE_PATH ?>js/boxEffect.js"></script>
<?php } ?>
<script src="<?php  echo INCLUDE_PATH ?>js/formularios.js"></script>

<?php
	if ( is_array($url) && strstr($url[0],'noticias') !== false) {
?>
<script>
	$('select').change(function(){
		location.href=include_path+"noticias/"+$(this).val();
	});
</script>
<?php } ?>

</body>
</html>